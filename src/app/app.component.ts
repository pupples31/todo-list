import { Component } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
  
})
export class AppComponent {
  
  isAuth = false ;
  

  itemsRef: AngularFireList<any>;

  items: Observable<any[]>;

  constructor(db: AngularFireDatabase,public afAuth: AngularFireAuth) {
    this.itemsRef = db.list('messages');
    // Use snapshotChanges().map() to store the key
    this.items = this.itemsRef.snapshotChanges().pipe(
      map(changes => 
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  ngOnInit(){
    this.afAuth.auth.onAuthStateChanged(function(user) {
      if (user) {
        console.log('change Connecté');
        this.isAuth = true;
      }else{
        console.log('change déconnecté');
        this.isAuth = false;
      }
    });
  }

  addItem(newName: any) {
    //console.log(this.afAuth.auth.currentUser);
    let author = "Anonymus"; 

    if(this.afAuth.auth.currentUser){
      author = this.afAuth.auth.currentUser.displayName;
    }
    this.itemsRef.push({ text: newName.value, author }); 
    newName.value='';
  }
  deleteItem(key: string) {    
    this.itemsRef.remove(key); 
  }

  login() {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }
  logout() {
    this.afAuth.auth.signOut();
  }

  loginWithEmail(email: any,password: any){

    if(email.value!='' && password.value!=''){
      console.log(email);
      console.log(password);
    
    this.afAuth.auth.createUserWithEmailAndPassword(email.value,password.value).then((data)=>{
                                                                                               console.log(data);
                                                                                               email.value ='';
                                                                                               password.value = '';
                                                                                              this.isAuth = true;

                                                                                              }

                                                                                    )
                                                                                .catch((reason)=>{
                                                                                                console.error(reason.message);

                                                                                              }
                                                                                    )

   
    }else{
      console.log("Champs non remplis");
    }
    
  }

  connexionWithEmail(email,password){

      this.afAuth.auth.signInWithEmailAndPassword(email.value,password.value).then((data)=>{
            console.log(data);
            this.isAuth = true;
            console.log(this.isAuth)
          })
          .catch((reason)=>{
            console.log(reason);
          })
  }

  deconnexionWithEmail(){

    this.afAuth.auth.signOut();
    this.isAuth = false;
  }



}